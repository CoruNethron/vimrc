set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
Bundle 'jistr/vim-nerdtree-tabs'
Plugin 'jdonaldson/vaxe'
Plugin 'flazz/vim-colorschemes'
Bundle 'lrvick/Conque-Shell'
Plugin 'tpope/vim-fugitive'
Plugin 'dracula/vim'
Plugin 'ruanyl/coverage.vim'

call vundle#end()            " required
filetype plugin indent on    " required

nmap <silent> <leader>t :NERDTreeTabsToggle<CR>
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

let g:coverage_json_report_path = 'artifacts/coverage.json'
let g:coverage_sign_covered = '$'
let g:coverage_interval = 5000

let g:nerdtree_tabs_open_on_console_startup=1
let g:airline_powerline_fonts = 1
set term=xterm-256color
set t_Co=256
set number
autocmd vimenter * silent! lcd %:p:h
